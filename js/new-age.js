(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 48)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

})(jQuery); // End of use strict


function redd() { // Funktion wird onClick auf den Button ausgeführt
  var id = $("#session").val(); // Selektiert das Eingabefeld
  if (isNaN(id)) {
    alert("Der Sitzungscode darf nur aus Zahlen bestehen!") // Prüft ob es NotANumber ist und gibt einen Fehler aus.
  }
  else if (id.length !== 8) {
    alert("Der Sitzungscode muss genau 8-Stellen besitzen!") // Prüft ob der Sitzungscode zu lange ist
  }
    else
    window.location.href = "https://frag.jetzt/participant/room/" + id; // Leitet an die Frag.Jetzt Webseite weiter mit der angegebenen Sitzungsnummer.
}

function showVid(state) {
  /** Benötigt als Parameter state der jeweils true oder false sein kann. Wird der Button zum Video einblenden gedrückt ist dieser auf true gesetzt.
   Ein eingebettetes YouTube wird nun anstelle des eigentlichen Textes angezeigt. Ein Ausblenden Button erscheint der den State false übergibt
   und wieder den ursprünglichen Text anzeigt. **/
  if (state === true) {
    $("#download").html("<div class=\"iframe-container\"><iframe id=\"ytplayer\" type=\"text/html\" width=\"560\" height=\"315\" src=\"https://www.youtube-nocookie.com/embed/qHRC6z9VtWs\" frameborder=\"0\" allowfullscreen ></iframe></div>" +
        "<br><a class=\"btn btn-outline btn-xl js-scroll-trigger\" onclick=\"showVid(false)\" id=\"demo-but\">Video ausblenden!</a>");

  }
  else {
    $("#download").html("<div class=\"container\">\n" +
        "      <div class=\"row\">\n" +
        "        <div class=\"col-md-8 mx-auto\">\n" +
        "          <h2 class=\"section-heading\">Die Zukunft in deinen Händen!</h2>\n" +
        "          <p>Frag.Jetzt funktioniert auch auf ihrem Mobiltelefon! Ohne Download. Komplett Kostenfrei.</p>\n" +
        "          <a class=\"btn btn-outline btn-xl js-scroll-trigger\" onclick=\"showVid(true)\" id=\"demo-but\">Demo anschauen!</a>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "    </div>");

  }
}
